// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// schema for the clients. required: true means it will raise exception otherwise
var clientSchema = new mongoose.Schema({
    _id:  {type: String, required: true},
    image: {type: String, required: true}
});

// Return model
module.exports = restful.model('Client', clientSchema);
