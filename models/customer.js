// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// schema for the customer. required: true means it will raise exception otherwise
var customerSchema = new mongoose.Schema({
    _id:  {type: String, required: true},
    name: {type: String, required: true},
    mobile: {type: String, required: true},
    email: {type: String, required: true},
//    signUpDate: {type: Date, required: true},
//    lastSignInDate: {type: Date, required: true},
    password: {type: String, required: true}
});

// Return model
module.exports = restful.model('Customer', customerSchema);